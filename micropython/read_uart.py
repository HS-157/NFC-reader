import time
import machine
import urequests
import json

with open('config.json', 'r') as file:
    config = json.load(file)

url = config['url']

import wifi
print('Connexion Wifi')
wifi.do_connect()

print("Connexion au port UART")
time.sleep(0.5)

uart = machine.UART(2, 115200, tx=17, rx=16)

print("Ok")

print("===========")

a = 0
while True:
    time.sleep(0.5)
    message_b = uart.readline()
    message = message_b.decode().replace('\r\n', '')

    print(message)
    if message.isdigit():
        data = {"id": message}
        # r = urequests.post(url, json=data)
        print("=== Id carte ! :", message, "===")
