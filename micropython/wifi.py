import json
import network

with open('config.json', 'r') as file:
    config = json.load(file)

def do_connect():
    sta_if = network.WLAN(network.STA_IF)
    if not sta_if.isconnected():
        print('Connexion au wifi en cours')
        sta_if.active(True)
        sta_if.connect(config['SSID'], config['password'])
        while not sta_if.isconnected():
            pass
    print('Configuration réseau :', sta_if.ifconfig())
